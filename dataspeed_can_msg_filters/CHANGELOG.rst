^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package dataspeed_can_msg_filters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2.0.5 (2024-10-08)
------------------

2.0.4 (2024-07-25)
------------------

2.0.3 (2023-07-11)
------------------

2.0.2 (2023-01-17)
------------------

2.0.1 (2022-02-09)
------------------
* Fix cmake problems and update for best practices
* Avoid deprecation warnings in Galactic
* Contributors: Kevin Hallenbeck, Micho Radovnikovich

2.0.0 (2021-11-02)
------------------
* Initial ROS2 release
* Contributors: Kevin Hallenbeck
