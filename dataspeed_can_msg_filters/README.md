# dataspeed_can_filters

Time synchronize multiple CAN messages to get a single callback

Similar to the ROS [ApproximateTime](http://wiki.ros.org/message_filters#ApproximateTime_Policy) message synchronization policy

See [dbw_ford_can/DbwNode](https://bitbucket.org/DataspeedInc/dbw_ros/src/foxy/dbw_ford_can/src/DbwNode.hpp) for example usage
