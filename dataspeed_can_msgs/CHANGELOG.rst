^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package dataspeed_can_msgs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

2.0.5 (2024-10-08)
------------------
* Add CAN-FD messages with fixed size data arrays
  Update DBC tools for new fixed sizes
  PlotJuggler can't import large amounts of messages with dynamic sized arrays
* Contributors: Kevin Hallenbeck

2.0.4 (2024-07-25)
------------------

2.0.3 (2023-07-11)
------------------
* Add dataspeed_can_msgs package for CAN-FD message
* Contributors: Kevin Hallenbeck
